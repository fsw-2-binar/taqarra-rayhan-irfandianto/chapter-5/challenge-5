const router = require('express').Router()
const Admin = require('../controllers/admin/carsControllers')
const uploader = require('../middlewares/uploader')
const Cars = require('../controller/carsController')

// API server
router.post('/api/cars', uploader.single('image'), Cars.createCars)

//Admin Client
router.get('/admin', Admin.homepage)
router.get('/admin/add', Admin.createPage)
router.post('/admin/add', uploader.single('foto'), Admin.createCars)
router.post('/admin/edit/:id', uploader.single('foto'), Admin.updateCars)
router.get('/admin/edit/:id', Admin.updatePage)
router.get('/admin/filter', Admin.filterPage)
router.post('/admin/delete/:id', Admin.deleteCar)
router.get('/admin/search', Admin.searchCars)

module.exports = router