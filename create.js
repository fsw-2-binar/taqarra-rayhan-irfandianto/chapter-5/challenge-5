const {mobil} = require('./models')

async function create(req,res){
  try{
    await mobil.create({
      nama:req.body.nama,
      harga: req.body.harga,
      approved : req.body.approved,
      createdAt: new Date(),
      updatedAt: new Date()
    })
    res.status(201).json({
      message:"succes"
    })
  }
  
  catch(error){
    res.status(500).send({
      message:error.message
    })
  }

}
// mobil.create({
//  nama: 'mclari',
//  harga: 2000000,
//  approved: true
// })
//  .then(mobil => {
//    console.log(mobil)
//    console.log("terakhir kali diupdate ->",mobil.dataValues.updatedAt)
//  })

module.exports = {
  create
}