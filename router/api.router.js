const express = require('express')
const router = express.Router()
const {tbl_size}= require('../models')
const {tbl_car}= require('../models')
const { getData,newCars,getDatabyid,deleteCars,editCars } = require('../controller/cars.controller')
const { getAllSize} = require('../controller/size.controller')

// router.use(expresslayouts);
// router.use(express.json())
// const { getAllSize } = require('../controllers/api/size.controller')

// async function getData(){
//     const carList = await tbl_size.findAll()
    
//      console.log("ini=-->",carList[0])
     
  


// }

// const data1= getData()
router.get('/cars', getData)
router.get('/size',getAllSize)
router.get('/cars/:id', getDatabyid)
// router.get('/size',(req,res)=>{
//     res.status(200).json({
//         message:'success',
//         data:[]
//     })
// })
router.post('/cars/new',newCars)
router.delete('/cars/delete/:id',deleteCars)
router.post('/cars/edit/:id',editCars)


module.exports=router