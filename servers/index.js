const express = require("express");

const expresslayouts = require("express-ejs-layouts");
const app = express();
const {mobil}= require('../models')
const {create} = require('../create')
const bp = require("body-parser");
const data = require("../service/service");
const webrouter= require('../router/web.router')
const apirouter =  require('../router/api.router')
// const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
// Ambil port dari environment variable
// Dengan nilai default 8000
const PORT = process.env.PORT || 8000;
app.use(expresslayouts);
app.use(express.json())
app.use(express.urlencoded())
// app.use(bp)
// app.use(bp.json())
app.use(express.json());
// app.use(bp.urlencoded({extended: true}));
app.use(express.static('public'))
app.use('/css',express.static(__dirname+ 'public/css'))
app.use('/img',express.static(__dirname+ 'public/image'))
app.use('/js',express.static(__dirname+ 'public/js'))
// app.use('/css',express.static(__dirname+ 'public/css'))


app.set('view engine', 'ejs')

// app.post("/mobil/create",create)

// app.get("/", data.getData)

app.use('/',webrouter)
app.use('/api',apirouter)
app.use('/edit-form',webrouter)


// app.get("/edit", (req, res) => {
//   res.render("partials/form", {
//     layout: "edit-form",
//     title: "challenge 5 lur",
//     // cars: mobil.findAll().then(mobil=>mobil)
//   });
//   // res.render('edit-form')
// })

app.listen(PORT, () => {
  console.log(`Server nyala di http://localhost:${PORT}`);
})
