'use strict';
const {tbl_size}= require('../models')
module.exports = {
  async up (queryInterface, Sequelize) {
    
   
     await queryInterface.bulkInsert('tbl_cars', [
      {
        name: "Xpander",
        harga: 150000,
        image: "https://assets.mitsubishi-motors.co.id/articles/1592478064-xpander-jpg.JPG",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:2
      },
      {
        name: "Avanza",
        harga: 130000,
        image: "https://awsimages.detik.net.id/visual/2022/01/21/toyota-all-new-avanza-2015-tangkapan-layar_169.jpeg?w=650",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:2

      },
      {
        name: "City",
        harga: 110000,
        image: "https://statik.tempo.co/data/2019/11/25/id_892307/892307_720.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        name: "kijank",
        harga: 170000,
        image: "https://www.toyota.astra.co.id/sites/default/files/2020-10/1_innova-super-white-2_0.png",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:2
      },
      {
        name: "crola",
        harga: 120000,
        image: "https://asset.kompas.com/crops/HxGhOuSBCDFE4wISZxK4kY8LNbg=/430x96:7896x5073/750x500/data/photo/2022/02/08/620210998b81f.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        name: "Ferguso",
        harga: 210000,
        image: "https://static.republika.co.id/uploads/images/inpicture_slide/mitsubishi-fuso-pimpin-pasar-kendaraan-niaga-di-jawa-timur-_160314205443-300.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:3
      },
      {
        name: "R3",
        harga: 145000,
        image: "https://suzukicdn.net/uploads/all-new-ertiga/front.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        name: "akord",
        harga: 145000,
        image: " https://www.hondapurwokerto.web.id/wp-content/uploads/2020/06/Harga-Honda-Accord-Indonesia-2020-2.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        name: "lancer",
        harga: 145000,
        image: " https://imgcdn.oto.com/medium/gallery/exterior/28/679/mitsubishi-lancer-evolution-1992-2007-21028.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        name: "fred",
        harga: 185000,
        image: " https://upload.wikimedia.org/wikipedia/commons/d/da/Honda_Freed_front.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:2
      }

      
     
    ], {});
    const user = await queryInterface.sequelize.query(
      `SELECT * from tbl_cars;`
    );
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
