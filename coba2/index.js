const express = require("express");

const app = express();

app.use(express.json());

const dataMarvel = require("./data/marvel");

function isAdmin(req, res, next) {
  if (req.query.iam === "admin") {
    next();
    return
  }

  res.status(401).send("Kamu bukan admin");
}

// app.get("/allData", isAdmin, dataMarvel.getallData);
// app.get("/data", isAdmin, dataMarvel.getData);
// app.post("/data", isAdmin, dataMarvel.createData);
// app.delete("/data", isAdmin, dataMarvel.deleteData);

function trueData(req, res, next) {
    const {id} = req.query;

    if (id) {
        next();
        return
    }  
    res.status(401).send("Data not found");
}

app.get("/allData", isAdmin, dataMarvel.getallData);
app.get("/data", trueData, dataMarvel.getData);
app.post("/data", trueData, dataMarvel.createData);
app.delete("/data", trueData, dataMarvel.deleteData);


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Express nyala di http://localhost:${PORT}`);
  });
  