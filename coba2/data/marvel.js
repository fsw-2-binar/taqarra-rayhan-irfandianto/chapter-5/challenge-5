const marvel = 
[
    { id: 1, title: "Morbius", studio: "Marvel", year: 2022 },
    { id: 2, title: "Dr. Strange", studio: "Marvel", year: 2016 },
    { id: 3, title: "Amazing Spiderman", studio: "Marvel", year: 2012 },
    { id: 4, title: "Iron Man", studio: "Marvel", year: 2008 },
    { id: 5, title: "Venom", studio: "Marvel", year: 2018 },
    { id: 6, title: "Cat Woman", studio: "DC", year: 2004 },
    { id: 7, title: "Batman Begins", studio: "DC", year: 2005 },
    { id:8, title: "Superman Return", studio: "DC", year: 2006 },
    { id:9, title: "Mulan", studio: "Walt Disney", year: 2020 },
    { id:10, title: "Aladdin", studio: "Walt Disney", year: 2019 },
  ];

  const getallData = (req, res) => {
      res.status(200).send(marvel);
  };

  const getData = (req,res) => {
      const {id} = req.query;
      const value = marvel.find((row)=> row.id==id);
      res.status(200).send(value)
  };

  const createData = (req, res) => {
      marvel.push(req.body);
      res.status(201).send("Data created");
  };

  const deleteData = (req, res) => {
      const {id} = req.query;
      const index = marvel.findIndex((row) => row.id == id);

      marvel.splice(index, 1);
      res.status(200).send(`Data with id ${id} was deleted`);
  }

  module.exports = {getallData, getData, createData, deleteData}