const {tbl_size}= require('../models')

const getAllSize = async (req, res) => {
    try{
        const sizeList = await tbl_size.findAll();
        res.status(200).json({
            message:'success',
            data: sizeList
              })
      }
      
      catch(error){
         
        res.status(500).send({
       
          message:error.message
        })
      }

}

module.exports = {
    getAllSize
}
