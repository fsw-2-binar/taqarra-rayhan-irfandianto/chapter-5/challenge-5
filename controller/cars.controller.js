
const {tbl_car}= require('../models')



    // const getData = async (req, res) => {
    //     const carList = await tbl_car.findAll();
    //     res.status(200).json({
    //         message:'success',
    //         data: carList
    //           })

    // }

    const getData = async (req, res) => {
        try{
            const carList = await tbl_car.findAll();
            res.status(200).json({
                message:'success',
                data: carList
                  })
          }
          
          catch(error){
             
            res.status(500).send({
           
              message:error.message
            })
          }

    }
    

    const getDatabyid = async (req, res) => {
      try{
          const carList = await tbl_car.findOne({
            where : {id : req.params.id}
          });
          res.status(200).json({
              message:'success',
              data: carList
                })
        }
        
        catch(error){
           
          res.status(500).send({
         
            message:error.message
          })
        }

  }
        
      
         
     

          
    const newCars = async (req, res) => {
        try{
            await tbl_car.create({
              name:  req.body.name,
              harga: req.body.harga,
              image: req.body.image,
              id_size: req.body.id_size
            }).then(()=>{   
              
res.redirect('/');

            })
            
          }
          
          catch(error){
              
            res.status(500).send({
           
              message:error.message
            })
          }

    }
    


    const editCars = async (req, res) => {
        try{
            const query = {
                where: { id:req.params.id }
               }
            
            await tbl_car.update({
                name: req.body.name,
                harga: req.body.harga,
                id_size:req.body.id_size,
                image:req.body.image 
               }, query).then(() => {
                 res.redirect("/")
                // res.send({name : "StackOverFlow", reason : "Need help!", redirect_path: "/"});
                })
           
            
          }
          
          catch(error){
             
            res.status(500).send({
           
              message:error.message
            })
          }

    }

    const deleteCars = async (req, res) => {
        try{
            await tbl_car.destroy({
              where:{
                  id:req.params.id
              }
            }).then(() => {
              res.redirect("/")
              // res.send({name : "StackOverFlow", reason : "Need help!", redirect_path: "/"});
           
            })
          }
          
          catch(error){
             
            res.status(500).send({
           
              message:error.message
            })
          }

    }
        




module.exports = {
    getDatabyid,
    newCars,
    editCars,
    getData,
    deleteCars
}
