const { Cars } = require('../models')
const imagekit = require('../lib/imageKit')

const createCars = async(req, res) => {
    const { nama, harga, ukuran } = req.body

    try {
        // untuk dapat extension file nya
        const split = req.file.originalname.split('.')
        const ext = split[split.length - 1]

        // upload file ke imagekit
        const img = await imagekit.upload({
            file: req.file.buffer, //required
            fileName: `${req.file.originalname}.${ext}`, //required
        })
        console.log(img.url)

        const newCars = await Cars.create({
            nama,
            harga,
            ukuran,
            image: img.url
        })

        res.status(201).json({
            status: 'success',
            data: {
                newCars
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}