# **Challenge 5 Manajemen Data Retal Mobil**


## **ERD **
 ![This is an image](https://res.cloudinary.com/dcxjkixo3/image/upload/v1650791613/ERD_zxydtk.png)

---

## **Route Web**
1. Index: http://localhost:8000
2. create: http://localhost:8000/add-form
3. update: http://localhost:8000/edit-form/{id}
## **Route API**
1. List Mobil: http://localhost:8000/api/cars
2. List Size: http://localhost:8000/api/size
3. Find Car by ID: http://localhost:8000/api/cars/:id
4. New Car: http://localhost:8000/api/cars/new
5. New Car: http://localhost:8000/api/cars/delete/:id



---

